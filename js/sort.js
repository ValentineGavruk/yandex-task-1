
//Обработка ошибок//
 var error = function (text) {
    alert(text);
    console.error(text);
};

if (typeof boardingCard === 'string'){
    boardingCard = JSON.parse(boardingCard);
}
//Проверка валидности данных//
if(typeof boardingCard !== 'object' && typeof boardingCard !== 'string'){
   error('Не верный формат данных!')
}

//Масив в котором будут хранится отсортированные билеты//
var tickets = [];

//Поиск первого билета.Начало пути//
var firstTicket = function (data) {
    var count = 0;
    data.forEach(function (ticket) {
        var sendFirstTicket = function (e) {
            return ticket.send.toLowerCase() !== e.arrival.toLowerCase();

        };
        if (data.every(sendFirstTicket)) {
            count++;
            if (count != 1) {
                return error('Не верно указаны данные маршрута!')
            }
            tickets.push(ticket);
        }
    });
};
//Поиск следующих билетов по маршруту.//
var nextTicket = function (data) {
    var number = tickets.length - 1;
    data.forEach(function (ticket) {
        if (ticket.send.toLowerCase() == tickets[number].arrival.toLocaleLowerCase()) {
            tickets.push(ticket);
            return nextTicket(data);
        }

    });
};

firstTicket(boardingCard);
nextTicket(boardingCard);

















