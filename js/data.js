/*
 Данные должны приходить в формате JSON или в виде массива объектов.Каждый объект - это отдельный билет.
 Данные в билете (свойства объекта):
 send - Место отправления;
 arrival - Место прибытие;
 transport - Транспорт передвижения;
 description - Описание,дополнительная информация о поездке.
 */



var boardingCard = '[' +
    '{' +
    '"send": "Питербург",' +
    ' "arrival": "Ростов",' +
    ' "transport": "Самолет",' +
    ' "description": "посадосное место 3A, номер ресйса 9567"' +
    '}' +
    ',{' +
    '"send": "Симферополь",' +
    ' "arrival": "Москва",' +
    ' "transport": "самолет",' +
    ' "description": "псадочное мето 9C"}, {' +
    '"send": "Москва", "arrival": "Питербург",' +
    ' "transport": "поезд",' +
    ' "description": "место 25, вагон 9"' +
    '},' +
    ' {' +
    '"send": "Ялта",' +
    ' "arrival": "Севастополь",' +
    ' "transport": "автобус",' +
    ' "description": "место 5"' +
    '},' +
    ' {' +
    '"send": "Севастополь",' +
    ' "arrival": "Симферополь",' +
    ' "transport": "автобус",' +
    ' "description": "место 9, номер автобуса 673"' +
    '},' +
    '{' +
    '"send": "Ростов",' +
    ' "arrival": "Краснодар",' +
    ' "transport": "самолет",' +
    ' "description": "посадочное место 5A,терминал А, номер рейса 5378"' +
    '}' +
    ']';